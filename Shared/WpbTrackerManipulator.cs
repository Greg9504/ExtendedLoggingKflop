﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Series;
using System.Collections.ObjectModel;

namespace ExtendedLoggingKflop.Shared
{
    public class WpbTrackerManipulator : MouseManipulator
    {
        /// <summary>
        /// The current series.
        /// </summary>
        private DataPointSeries currentSeries;

        public WpbTrackerManipulator(IPlotView plotView)
            : base(plotView)
        {
        }

        /// <summary>
        /// Occurs when a manipulation is complete.
        /// </summary>
        /// <param name="e">
        /// The <see cref="OxyPlot.OxyMouseEventArgs" /> instance containing the event data.
        /// </param>
        public override void Completed(OxyMouseEventArgs e)
        {
            base.Completed(e);
            e.Handled = true;

            currentSeries = null;
            PlotView.HideTracker();
        }

        /// <summary>
        /// Occurs when the input device changes position during a manipulation.
        /// </summary>
        /// <param name="e">
        /// The <see cref="OxyPlot.OxyMouseEventArgs" /> instance containing the event data.
        /// </param>
        public override void Delta(OxyMouseEventArgs e)
        {
            base.Delta(e);
            e.Handled = true;

            if (currentSeries == null || XAxis == null)
            {
                PlotView.HideTracker();
                return;
            }

            var actualModel = PlotView.ActualModel;
            if (actualModel == null)
            {
                return;
            }

            if (!actualModel.PlotArea.Contains(e.Position.X, e.Position.Y))
            {
                return;
            }
            
            var time = currentSeries.InverseTransform(e.Position).X;
            var points = currentSeries.Points;
            DataPoint dp = points.FirstOrDefault(d => d.X >= time);
            time = dp.X;
            StringBuilder hovertext = new StringBuilder(string.Format("Time: {0:0.0000}\n", time));
            // Exclude default DataPoint.
            // It has insignificant downside and is more performant than using First above
            // and handling exceptions.
            if (dp.X != 0 || dp.Y != 0)
            {
                int index = points.IndexOf(dp);
                var ss = PlotView.ActualModel.Series.Cast<DataPointSeries>();
                double[] values = new double[ss.Count()];
                int i = 0;
                foreach (var series in ss)
                {
                    var seriespoints = series.Points;
                    DataPoint seriesdp = seriespoints.FirstOrDefault(d => Math.Abs(d.X - time) <= 0.0000001);
                    values[i] = seriesdp.Y;
                    // values[i] = (series.Points)[index].Y;
                    hovertext.Append(string.Format("{0}: {1:0.######}\n", series.Title, values[i]));
                    i++;
                }

                var position = XAxis.Transform(dp.X, dp.Y, currentSeries.YAxis);
                position = new ScreenPoint(position.X, position.Y);// e.Position.Y);

                var result = new WpbTrackerHitResult(values)
                {
                    Series = currentSeries,
                    DataPoint = dp,
                    Index = index,
                    Item = dp,
                    Position = position,
                    PlotModel = PlotView.ActualModel,
                    Text = hovertext.ToString()
                };
                PlotView.ShowTracker(result);
            }
        }

        /// <summary>
        /// Occurs when an input device begins a manipulation on the plot.
        /// </summary>
        /// <param name="e">
        /// The <see cref="OxyPlot.OxyMouseEventArgs" /> instance containing the event data.
        /// </param>
        public override void Started(OxyMouseEventArgs e)
        {
            base.Started(e);
            var mousedown = e as OxyPlot.OxyMouseDownEventArgs;
            if (mousedown != null)
            {
                currentSeries = mousedown.HitTestResult?.Element as OxyPlot.Series.LineSeries;
            }
            else
            {
                currentSeries = null;
            }
            // currentSeries = PlotView?.ActualModel?.Series
            //                 .FirstOrDefault(s => s.IsVisible) as DataPointSeries;
            Delta(e);
        }
    }
}
