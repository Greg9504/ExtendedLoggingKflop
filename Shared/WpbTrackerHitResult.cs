﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;

namespace ExtendedLoggingKflop.Shared
{
    public class WpbTrackerHitResult : TrackerHitResult
    {
        public double[] Values { get; private set; }

        // can't use the default indexer name (Item) since the base class uses that for something else
        [System.Runtime.CompilerServices.IndexerName("ValueString")]
        public string this[int index]
        {
            get
            {
                return string.Format((index == 1 || index == 4) ?
                  "{0,7:###0   }" : "{0,7:###0.0#}", Values[index]);
            }
        }

        public WpbTrackerHitResult(double[] values)
        {
            Values = values;
        }
    }
}
