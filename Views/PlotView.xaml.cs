﻿using OxyPlot;
using ExtendedLoggingKflop.Shared;

namespace ExtendedLoggingKflop.Views
{   
    using Catel.Windows;
    public partial class PlotView:Window
    {
        public PlotView() : base()
        {
            InitializeComponent();
            this.Owner = null;//this allows the main window to cover the plot            
            var pc = this._oxyPlot.ActualController;
            pc.UnbindMouseDown(OxyMouseButton.Left);
            pc.UnbindMouseDown(OxyMouseButton.Left, OxyModifierKeys.Control);
            pc.UnbindMouseDown(OxyMouseButton.Left, OxyModifierKeys.Shift);

            pc.BindMouseDown(OxyMouseButton.Left, new DelegatePlotCommand<OxyMouseDownEventArgs>(
                         (view, controller, args) =>
                            controller.AddMouseManipulator(view, new WpbTrackerManipulator(view), args)));
        }
    }
}
